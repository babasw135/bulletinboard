package com.example.bulletinboard.controller;

import com.example.bulletinboard.model.BulletinBoardChangeRequest;
import com.example.bulletinboard.model.BulletinBoardRequest;
import com.example.bulletinboard.model.BulletinBoardResponse;
import com.example.bulletinboard.model.BulletinBoarditem;
import com.example.bulletinboard.repository.BulletinBoardRepository;
import com.example.bulletinboard.service.BulletinBoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/BulletinBoar")
 class BulletinBoardcontroller {
    private final BulletinBoardService bulletinBoardService;

    @PostMapping("/new")
    public String setBulletinBoard(@RequestBody BulletinBoardRequest request){
        bulletinBoardService.setBulletinBoard(request);

        return "ok";
    }
    @GetMapping("/moo")
    public List<BulletinBoarditem> getitem(){
    return bulletinBoardService.getitem();


    }
    @GetMapping("/mee/{id}")
    public BulletinBoardResponse getBulletinBoard( @PathVariable long id){
        return bulletinBoardService.getBulletinBoard(id);
    }
    @PutMapping("/maa/{id}")
    public String putBulletinBoard(@PathVariable long id,@RequestBody BulletinBoardChangeRequest request){
        bulletinBoardService.putBulletinBoard(id, request);

        return "ok";
    }
    @DeleteMapping("/{id}")
    public String delBulletinBoard(@PathVariable long id){
        bulletinBoardService.delBulletinBoard(id);

        return "ok";
    }
}
