package com.example.bulletinboard.service;

import com.example.bulletinboard.Entity.BulletinBoard;
import com.example.bulletinboard.model.BulletinBoardChangeRequest;
import com.example.bulletinboard.model.BulletinBoardRequest;
import com.example.bulletinboard.model.BulletinBoardResponse;
import com.example.bulletinboard.model.BulletinBoarditem;
import com.example.bulletinboard.repository.BulletinBoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
    public class BulletinBoardService {
        private final BulletinBoardRepository bulletinBoardRepository;

    public void setBulletinBoard(BulletinBoardRequest request){
        BulletinBoard addData = new BulletinBoard();
        addData.setMovieTitle(request.getMovieTitle());
        addData.setRank(request.getRank());
        addData.setContent(request.getContent());
        addData.setDay(LocalDate.now());
        addData.setMemo(request.getMemo());

        bulletinBoardRepository.save(addData);
    }
    public List<BulletinBoarditem> getitem(){
        List<BulletinBoard> originList = bulletinBoardRepository.findAll();

        List<BulletinBoarditem> result = new LinkedList<>();

        for (BulletinBoard bulletinBoard : originList) {
            BulletinBoarditem additem = new BulletinBoarditem();
            additem.setId(bulletinBoard.getId());
            additem.setMovieTitle(bulletinBoard.getMovieTitle());
            additem.setRank(bulletinBoard.getRank().getName());
            additem.setContent(bulletinBoard.getContent());
            additem.setDay(bulletinBoard.getDay());
            additem.setMemo(bulletinBoard.getMemo());

            result.add(additem);
        }
        return result;
    }
    public BulletinBoardResponse getBulletinBoard(long id){
        BulletinBoard originData = bulletinBoardRepository.findById(id).orElseThrow();

        BulletinBoardResponse response = new BulletinBoardResponse();
        response.setId(originData.getId());
        response.setMovieTitle(originData.getMovieTitle());
        response.setRank(originData.getRank().getName());
        response.setContent(originData.getContent());
        response.setDay(originData.getDay());
        response.setMemo(originData.getMemo());

        return response;
    }
    public void putBulletinBoard(long id,BulletinBoardChangeRequest request){
        BulletinBoard originData = bulletinBoardRepository.findById(id).orElseThrow();
        originData.setRank(request.getRank());

        bulletinBoardRepository.save(originData);
    }
    public void delBulletinBoard(long id){
        bulletinBoardRepository.deleteById(id);
    }
}
