package com.example.bulletinboard.model;

import com.example.bulletinboard.Enums.BulletinBoardEnums;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BulletinBoardRequest {
    private String movieTitle;
    @Enumerated(value = EnumType.STRING)
    private BulletinBoardEnums rank;
    private String content;
    private LocalDate Day;
    private String memo;
}
