package com.example.bulletinboard.model;


import com.example.bulletinboard.Enums.BulletinBoardEnums;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BulletinBoardChangeRequest {
    @Enumerated(value = EnumType.STRING)
    private BulletinBoardEnums rank;
}
