package com.example.bulletinboard.model;

import com.example.bulletinboard.Enums.BulletinBoardEnums;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BulletinBoardResponse {
    private Long id;
    private String movieTitle;
    private String rank;
    private String content;
    private LocalDate Day;
    private String memo;
}
