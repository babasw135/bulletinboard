package com.example.bulletinboard.Entity;


import com.example.bulletinboard.Enums.BulletinBoardEnums;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class BulletinBoard {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String movieTitle;
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private BulletinBoardEnums rank;
    @Column(nullable = false)
    private String content;
    @Column(nullable = false)
    private LocalDate Day;
    @Column(columnDefinition = "TEXT")
    private String memo;

}
