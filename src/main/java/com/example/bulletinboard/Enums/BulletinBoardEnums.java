package com.example.bulletinboard.Enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BulletinBoardEnums {
    RATING1("S급"),
    RATING2("A급"),
    RATING3("B급"),
    RATING4("C급");

    private final String name;
}
