package com.example.bulletinboard.repository;

import com.example.bulletinboard.Entity.BulletinBoard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BulletinBoardRepository extends JpaRepository<BulletinBoard, Long> {
}
